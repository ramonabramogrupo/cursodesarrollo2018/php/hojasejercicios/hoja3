<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        /**
         * Esta funcion genera una serie de numeros aleatorios
         * @param int $minimo Este es el valor minimo
         * @param int $maximo Este es el valor maximo
         * @param int $numero Numero de valores a generar
         * @param int[] $salida Es el array donde se almacenara la salida
         * @return void
         * 
         */
        function ejercicio2($minimo, $maximo, $numero, &$salida) {

            /*
             * Inicializo el array para evitar que se quede con valores anteriores
             */
            $salida = array();

            /*
             * Bucle para rellenar el array
             */
            for ($c = 0; $c < $numero; $c++) {
                $salida[$c] = mt_rand($minimo, $maximo);
            }
        }

        $salida[30] = 1;
        ejercicio2(1, 10, 10, $salida);
        var_dump($salida);
        
        ?>
    </body>
</html>
