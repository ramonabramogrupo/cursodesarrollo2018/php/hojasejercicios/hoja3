<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que le pasas un array con valores y ella te devuelve las veces que se repite cada valor
         * @param array $array es el conjunto de valores a utilizar
         * @param bool $devolverTodos si le indicas true te devuelve los valores que no se repiten tambien
         * @return int[] es el array con las frecuencias de cada uno de los valores del array de entrada
         */
        function elementosRepetidos($array, $devolverTodos = false) {
            $repeated = array();

            foreach ((array) $array as $value) {
                $inArray = false;

                foreach ($repeated as $i => $rItem) {
                    if ($rItem['value'] === $value) {
                        $inArray = true;
                        ++$repeated[$i]['count'];
                    }
                }

                if (false === $inArray) {
                    $i = count($repeated);
                    $repeated[$i] = array();
                    $repeated[$i]['value'] = $value;
                    $repeated[$i]['count'] = 1;
                }
            }

            if (!$devolverTodos) {
                foreach ($repeated as $i => $rItem) {
                    if ($rItem['count'] === 1) {
                        unset($repeated[$i]);
                    }
                }
            }

            sort($repeated);

            return $repeated;
        }
        
        $entrada=array(1,2,3,"a","a","b",1,1,1,1,1);
        var_dump(elementosRepetidos($entrada,TRUE));
        
        
        
        ?>
    </body>
</html>
