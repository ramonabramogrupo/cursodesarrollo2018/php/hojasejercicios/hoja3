<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        /**
         * Funcion que genera colores
         * @param int $numero El numero de colores a generar
         * @param bool $almohadilla=true con valor true nos indica que coloquemos la almohadilla
         * @return array los colores solicitados en un array de cadenas
         */
        
        function generaColores($numero,$almohadilla=true){
            $colores=array();
            for($n=0;$n<$numero;$n++){
                $c=0;
                $limite=6;
                $colores[$n]="";
                if($almohadilla){
                    $colores[$n]="#";
                    $limite=7;
                }
                for(;$c<$limite;$c++){
                    $colores[$n].=dechex(mt_rand(0,15));
                }
            }
            return $colores;
        }

            var_dump(generaColores(10,false));
            
        ?>
    </body>
</html>
